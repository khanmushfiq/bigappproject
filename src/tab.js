import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";

import App from "./App";
import SignIn from "./sign-in";
import OrderList from "./user-list";

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired
};

const styles = theme => ({
  root: {
    margin: "auto",
    paddingTop: "1em",
    flexGrow: 1,
    width: "80%",
    backgroundColor: theme.palette.background.paper
  }
});

class MenuTab extends React.Component {
  state = {
    value: 0
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes } = this.props;
    const { value } = this.state;

    return (
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="scrollable"
            scrollButtons="auto"
          >
            <Tab label="Log In" />
            <Tab label="User list" />
            <Tab label="Drag and Drop" />
          </Tabs>
        </AppBar>
        {value === 0 && <TabContainer>{<SignIn />}</TabContainer>}
        {value === 1 && <TabContainer>{<OrderList />}</TabContainer>}
        {value === 2 && <TabContainer>{<App />}</TabContainer>}
      </div>
    );
  }
}

MenuTab.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(MenuTab);
