import "semantic-ui-css/semantic.min.css";
import React, { Component } from "react";
import { Icon, List, Segment } from "semantic-ui-react";

let l_arr = [];
let r_arr = [];
let arr = [
  "An excellent companion",
  "A poodle, its pretty basic",
  "An excellent polish restaurant",
  "A taste of Shaanxi's delicious culinary tradition",
  "Greenpoint's best choice for quick and delicious sushi.",
  "At night a bar, during the day a delicious brunch spot.",
  "Quick delivery and hearty, filling meals",
  "With delights like spicy cold noodles and lamb burgers",
  "Sometimes can be a lovely city",
  "Has not watched anything recently"
];
class App extends Component {
  state = {
    left_divs: arr,
    right_divs: [],
    current: "notSelected"
  };
  onLiClicked = (e, i) => {
    this.setState({ current: i });
  };
  componentDidMount() {
    l_arr = [...this.state.left_divs];
    r_arr = [...this.state.right_divs];
  }
  onInputChanged = e => {
    let value = e.target.value;
    let name = e.target.name;
    let right_divs1 = [];
    let left_divs1 = [];
    if (e.target.value != "") {
      let right_divs1 = [...this.state.right_divs];
      let left_divs1 = [...this.state.left_divs];
    }
    console.log(this.state.left_divs);

    if (e.target.value == "") {
      if (name === "left") this.setState({ left_divs: l_arr });
      else {
        this.setState({ right_divs: r_arr });
      }
    } else {
      l_arr = [...left_divs1];
      r_arr = [...right_divs1];
      if (name === "left") {
        if (left_divs1.length !== 0)
          left_divs1 = left_divs1.filter(r =>
            r.toLowerCase().startsWith(value.toLowerCase())
          );
      } else {
        if (right_divs1.length !== 0)
          right_divs1 = right_divs1.filter(r =>
            r.toLowerCase().startsWith(value.toLowerCase())
          );
      }
      this.setState({
        right_divs: right_divs1,
        left_divs: left_divs1
      });
    }
  };
  onUnshiftClicked = () => {
    if (this.state.current !== "notSelected") {
      let right_divs1 = [...this.state.right_divs];
      let left_divs1 = [...this.state.left_divs];
      right_divs1.push(left_divs1[this.state.current]);
      left_divs1.splice(this.state.current, 1);
      this.setState({
        right_divs: right_divs1,
        left_divs: left_divs1,
        current: "notSelected"
      });
    } else {
      alert("Please select a tag");
    }
  };
  onShiftClicked = () => {
    if (this.state.current !== "notSelected") {
      let right_divs1 = [...this.state.right_divs];
      let left_divs1 = [...this.state.left_divs];
      left_divs1.push(right_divs1[this.state.current]);
      right_divs1.splice(this.state.current, 1);
      this.setState({
        right_divs: right_divs1,
        left_divs: left_divs1,
        current: "notSelected"
      });
    } else {
      window.alert("Please select a tag");
    }
  };
  render() {
    return (
      <div style={{ display: "flex" }}>
        <div style={{ flex: "8" }}>
          <Segment>
            {this.state.left_divs.map((a, i) => {
              return (
                <Segment key={i} onClick={e => this.onLiClicked(e, i)}>
                  <List divided inverted relaxed>
                    <List.Item>
                      <List.Content>{a}</List.Content>
                    </List.Item>
                  </List>
                </Segment>
              );
            })}
          </Segment>
        </div>
        <div style={{ flex: "1", textAlign: "center", marginTop: "15%" }}>
          <div>
            <div onClick={this.onUnshiftClicked}>
              <Icon name="arrow alternate circle right" size="big" />
            </div>
            <div onClick={this.onShiftClicked} style={{ padding: "1em" }}>
              <Icon name="arrow alternate circle left" size="big" />
            </div>
          </div>
        </div>
        <div style={{ flex: "8" }}>
          <Segment>
            {this.state.right_divs.map((a, i) => {
              return (
                <Segment key={i} onClick={e => this.onLiClicked(e, i)}>
                  <List divided inverted relaxed>
                    <List.Item>
                      <List.Content>{a}</List.Content>
                    </List.Item>
                  </List>
                </Segment>
              );
            })}
          </Segment>
        </div>
      </div>
    );
  }
}

export default App;
